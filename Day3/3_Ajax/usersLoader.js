var root = 'https://jsonplaceholder.typicode.com',
    $fetchDataBtn = $('.fetchData'),
    $tableBody = $('.data tbody'),
    openWeatherAppId = 'a1c52306b2406040f1763904d7f0163e',
    plovdivLat = 42.15,
    plovdivLong = 24.75,
    $weatherLocation = $('.weatherLocationName'),
    $weatherIcon = $('.weatherIcon'),
    $weatherDescription = $('.weatherDescription'),
    $weatherTemp = $('.weatherTemp'),
    $weatherBlock = $('.weatherInformation'),
    $getWeatherBtn = $('.getWeather');

$fetchDataBtn.on('click',
    function() {
        $.ajax({
            url: root + '/users',
            method: 'GET'
        }).then(function(data) {
            clearTableBody();
            parseUsers(data);
        });
    });

function parseUsers(data){
    for (let i = 0; i < data.length; i++) {
        writeUserToTable(data[i]);
    }
}
function clearTableBody() {
    $tableBody.text('');
}
function writeUserToTable(user) {
    let row =
        '<tr><td>' + user.id +
        '</td><td>' + user.name +
        '</td><td>' + user.username +
        '</td><td>' + user.email +
        '</td><td>' + user.address.city + ' ' + user.address.street +
        '</td><td>' + user.phone +
        '</td><td>' + user.website +
        '</td><td>' + user.company.name + '</td></tr>';
    
    $tableBody.append(row);
}

$getWeatherBtn.on('click',
    function() {
        getWeatherByCoordinates(plovdivLat, plovdivLong);
    }
    );
function getWeatherByCoordinates(lat, long) {
    $.ajax({
        url: 'http://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + long + '&appid=' + openWeatherAppId,
        method: 'GET'
    }).then(function (data) {
        writeWeather(data);
    })
}
function writeWeather(data) {
    $weatherLocation.text(data.name);

    $weatherIcon.attr('src', 'http://openweathermap.org/img/w/' + data.weather[0].icon + '.png');

    let description = data.weather[0].description[0].toUpperCase() + data.weather[0].description.substr(1);
    $weatherDescription.text(description);

    $weatherTemp.text(Math.floor(data.main.temp / 10))

    $getWeatherBtn.fadeOut();
    $weatherBlock.fadeIn();
}