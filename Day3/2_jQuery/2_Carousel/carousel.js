var box = $('.carouselContent'),
    next = $('.rightBtn'),
    prev = $('.leftBtn'),
    items = $('.carouselItems li'),
    counter = 0,
    amount = items.length,
    current = $(items[0]);
box.addClass('active');
console.log(prev);

function navigate(direction) {
    current.removeClass('current');
    current.css('display', 'none');
    counter = counter + direction;
    if (direction === -1 &&
        counter < 0) {
        counter = amount - 1;
    }
    if (direction === 1 &&
        !items[counter]) {
        counter = 0;
    }
    current = $(items[counter]);
    current.fadeIn();
    current.addClass('current');
}

next.on('click', function (ev) {
    navigate(1);
});
prev.on('click', function (ev) {
    navigate(-1);
});
navigate(0);
