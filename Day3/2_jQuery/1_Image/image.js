/**
 * Created by princ on 21/07/2017.
 */

var $img = $('img#image');
var $toggleViewBtn = $('#toggleView'),
    $toggleBordersBtn = $('#toggleBorders'),
    $toggleRedBorderBtn = $('#toggleRedBorder'),
    $moveUpBtn = $('#moveUp'),
    $moveDownBtn = $('#moveDown'),
    $imageWrapper = $('#imageWrapper');


$toggleViewBtn.on('click', function () {
    if ($img.css('display') == 'none') {
        $img.fadeIn();
        $toggleViewBtn.text("Hide");
    } else {
        $img.fadeOut();
        $toggleViewBtn.text("Show");
    }
});

$toggleBordersBtn.on('click', function () {
        if ($img.css('border-radius') == '50%') {
            $img.animate({
                'border-radius': '0'
            }, 500)
            $toggleBordersBtn.text("Round");
        }
        else {
            $img.animate({
                'border-radius': '50%'
            }, 500)
            $toggleBordersBtn.text("Straighten");
        }
    }
);

$toggleRedBorderBtn.on('click', function(){
    if($img.hasClass('imgBorder')){
        $img.removeClass('imgBorder')
    }
    else {
        $img.addClass('imgBorder')
    }
});

$moveUpBtn.on('click', function(){
    $imageWrapper.animate({
        marginTop: "-=10px"
    }, 200)
});
$moveDownBtn.on('click', function(){
    $imageWrapper.animate({
        marginTop: "+=10px"
    }, 200)
});

