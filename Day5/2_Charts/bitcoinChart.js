var apiRoot = "https://api.coindesk.com/v1/bpi/historical/close.json"
loadRecent(10);
function loadRecent(days){
    var today = new Date(),
        startDate = new Date(new Date().setDate(today.getDate() - days)),
        s = '-';
    var chartData = {};
    $.ajax({
        url: apiRoot + '?start=' + startDate.getFullYear() + s + ("0" + startDate.getMonth()).slice(-2) + s + ("0" + startDate.getDate()).slice(-2) + '&end=' + today.getFullYear() + s + ("0" + today.getMonth()).slice(-2) + s + ("0" + today.getDate()).slice(-2),
        method: 'GET'
    }).then(function(data) {
        data = JSON.parse(data)
        var series = [];
        for(var key in data["bpi"]){
            series.push(data.bpi[key]);
        }
        chartData = {
            labels: getLabels(startDate, days),
            series: [series]
        }
        makeChart(chartData);
    });
    return chartData;
}

function getLabels(startDate, days){
    var labels = [("0" + startDate.getDate()).slice(-2) + '-' + ("0" + startDate.getMonth()).slice(-2)];
    for(var i = 0; i < days; i++){
        var date = new Date(startDate.setDate(startDate.getDate() - 1));
        labels.push(("0" + date.getDate()).slice(-2) + '-' + ("0" + date.getMonth()).slice(-2))
    }

    return labels;
}

function makeChart(data){
    var chart = new Chartist.Line('.ct-chart', data, {
        low: 0,
        showArea: true,
        showPoint: false,
        fullWidth: true
    });

    chart.on('draw', function(data) {
        if(data.type === 'line' || data.type === 'area') {
            data.element.animate({
                d: {
                    begin: 2000 * data.index,
                    dur: 2000,
                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                    to: data.path.clone().stringify(),
                    easing: Chartist.Svg.Easing.easeOutQuint
                }
            });
        }
    });
}

google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Pie Flavor', 'Favorites (in million)'],
        ['Apple', 47], ['Pumpkin', 37], ['Chocolate creme', 32],
        ['Cherry', 27], ['Apple crumb', 25], ['Pecan', 24],
        ['Lemon meringue', 24], ['Blueberry', 21], ['Key lime', 18],
        ['Peach', 16]
    ]);

    var options = {
        title: 'Top Pie Flavors',
        legend: 'none',
        pieSliceText: 'label',
        slices: {
            4: {offset: 0.1},
            9: {offset: 0.3},
            8: {offset: 0.4}
        },
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
}
